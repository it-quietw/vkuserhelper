const { VK, Keyboard } = require('vk-io')

const config = require('./config.json')

const groupVK = new VK({ "token": config.group.token });
const userVK = new VK({"token": config.user.token});

groupVK.updates.hear(/^Начать|^Главное меню/i, async(context) => {
    if(await isFromMe(context)) {
        const builder = Keyboard.builder();
        builder
            .textButton({
                label: 'Сообщения',
                color: Keyboard.POSITIVE_COLOR
            })
            .row()
            .textButton({
                label: 'Настройки бота',
                color: Keyboard.NEGATIVE_COLOR
            })
            .inline(true)
        await groupVK.api.messages.send({
            message: "Главное меню.",
            keyboard: builder,
            random_id: 0,
            peer_id: context.peerId
        })
    }
});

groupVK.updates.hear(/^Сообщения/i, async (context) => {
    if(await isFromMe(context)) {
        const builder = Keyboard.builder();
        builder
            .textButton({
                label: 'Отметить всё как прочитанное',
                color: Keyboard.POSITIVE_COLOR
            })
            .row()
            .textButton({
                label: 'Главное меню',
                color: Keyboard.PRIMARY_COLOR
            })
            .row()
            .inline(true)
        await groupVK.api.messages.send({
            message: "Меню сообщений.",
            keyboard: builder,
            random_id: 0,
            peer_id: context.peerId
        })
    }
});

groupVK.updates.hear(/^Настройки бота/i, async (context) => {
    if(await isFromMe(context)) {
        const builder = Keyboard.builder();
        builder
            .textButton({
                label: 'Удалить клавиатуру',
                color: Keyboard.POSITIVE_COLOR
            })
            .row()
            .textButton({
                label: 'Главное меню',
                color: Keyboard.PRIMARY_COLOR
            })
            .row()
            .inline(true)
        await groupVK.api.messages.send({
            message: "Управление ботом.",
            keyboard: builder,
            random_id: 0,
            peer_id: context.peerId
        })
    }
});

groupVK.updates.hear(/^Отметить всё как прочитанное/i, async (context) => {
   if(await isFromMe(context)) {
       const userUnreadConversations = await userVK.api.messages.getConversations({filter: "unread"});
       let chatNames = [];
       for(let unreadConversation in userUnreadConversations.items) {
           if(userUnreadConversations.items.hasOwnProperty(unreadConversation)) {
               if(userUnreadConversations.items[unreadConversation]["conversation"]["peer"]["type"] === "chat") {
                   chatNames.push(userUnreadConversations.items[unreadConversation]["conversation"]["chat_settings"]["title"]);
               }
               else if(userUnreadConversations.items[unreadConversation]["conversation"]["peer"]["type"] === "user") {
                   chatNames.push(`vk.com/id${userUnreadConversations.items[unreadConversation]["conversation"]["peer"]["id"]}`);
               }
           }
       }
       await context.send(`Диалоги найдены, начинаю очистку!
   Названия чатов:
   [${userUnreadConversations.count} чатов] ${chatNames.join(', ')}`);
       // console.log(userUnreadConversations.items[Math.floor(Math.random() * userUnreadConversations.items.length)]["conversation"]["peer"]["id"]);
       for(let unreadConversation in userUnreadConversations.items) {
           if(userUnreadConversations.items.hasOwnProperty(unreadConversation)) {
               await userVK.api.messages.markAsRead({peer_id: userUnreadConversations.items[unreadConversation]["conversation"]["peer"]["id"]})
           }
       }
       const builder = Keyboard.builder();
       builder
           .textButton({
               label: 'Сообщения',
               color: Keyboard.POSITIVE_COLOR
           })
           .row()
           .textButton({
               label: 'Настройки бота',
               color: Keyboard.NEGATIVE_COLOR
           })
           .inline(true)
       await groupVK.api.messages.send({
           message: "Все диалоги отмечены как прочитанные!",
           keyboard: builder,
           random_id: 0,
           peer_id: context.peerId
       })
   }
});

groupVK.updates.hear(/^Удалить клавиатуру/i, async (context) => {
    if(await isFromMe(context)) {
        const builder = Keyboard.builder();
        await groupVK.api.messages.send({
            message: "Клавиатура удалена!",
            keyboard: builder,
            random_id: 0,
            peer_id: context.peerId
        })
    }
});

const isFromMe = async (context) => {
    const senderId = context.senderId;
    let userId = await userVK.api.users.get({});
    return senderId === userId[0].id;
};

console.log(groupVK.api.API_VERSION)
groupVK.updates.start().catch(console.log)